# -*- coding:utf-8 -*-
import os

BASE_DIR = os.path.join(os.path.dirname(__file__))
#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR,"app.db")
SQLALCHEMY_DATABASE_URI = "mysql://root:beibei@192.168.8.29:3306/app"
SQLALCHEMY_MIGRATE_REPO = os.path.join(BASE_DIR, 'db.repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False
CSRF_ENABLED = True
SECRET_KEY = 'g~\x92!\xcf\x88\xb8\xfb|n\xd4\x86d[\x10\xd9\x0c\x1e\x15\xef*T\x91:'
OPENID_PROVIDERS = [
    {'name': 'Google', 'url': 'https://www.google.com/accounts/o8/id'},
    {'name': 'Yahoo', 'url': 'https://me.yahoo.com'},
    {'name': 'AOL', 'url': 'http://openid.aol.com/<username>'},
    {'name': 'Flickr', 'url': 'http://www.flickr.com/<username>'},
    {'name': 'MyOpenID', 'url': 'https://www.myopenid.com'}]


MAIL_SERVER = "localhost"
MAIL_PORT = 125
MAIL_USERNAME = ""
MAIL_PASSWORD = ""
ADMINS = ["qinzhou_zheng@126.com"]

POSTS_PER_PAGE = 3
WHOOSH_BASE = os.path.join(BASE_DIR, 'search.db')
MAX_SEARCH_RESULTS = 50

LANGUAGES = {
    "en": "English",
    'zh': "Chinese",
}