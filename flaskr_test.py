#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Author: Atom
    Date: 2016/9/6
    Time: 15:23
"""

import os
import tempfile
import unittest

import flaskr


class FlaskrTestCase(unittest.TestCase):
    def setUp(self):
        self.db_fd, flaskr.app.config['DATABASE'] = tempfile.mkstemp()
        flaskr.app.config['TESTING'] = True
        self.app = flaskr.app.test_client()
        with flaskr.app.app_context():
            flaskr.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(flaskr.app.config['DATABASE'])

    def test_empty_db(self):
        rv = self.app.get('/')
        assert b'No entries here so far' in rv.data


    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_login_logout(self):
        rv = self.login('admin','admin')
        assert "You were logged in" in rv.data

        rv = self.logout()
        assert 'You were logged out' in rv.data

        rv = self.login('adminx','admin')
        assert "Invalid username" in rv.data

        rv = self.login('admin','adminx')
        assert 'Invalid password' in rv.data

if __name__ == "__main__":
    unittest.main()
