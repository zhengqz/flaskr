#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/19
    Time: 9:44
"""
from datetime import datetime

from flask import render_template, redirect, g, session, url_for, flash, request
from flask_login import login_user, logout_user, current_user, login_required
from flask_babel import gettext
from app import app,babel
from app import lm, oid, db
from .forms import LoginForm, EditForm, PostForm, SearchForm
from .models import User, Post


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
@app.route("/index/<int:page>", methods=["GET", "POST"])
@login_required
def index(page=1):
    form = PostForm()
    if form.validate_on_submit():
        post = Post(body=form.post.data, timestamp=datetime.utcnow(), author=g.user)
        db.session.add(post)
        db.session.commit()
        flash("Your post is now live!")
        return redirect(url_for("index"))
    posts = [
        {
            "author": {"nickname": "John"},
            "body": "Beautiful day in Portland!"
        },
        {
            "author": {"nickname": "Susan"},
            "body": "The Avengers movie was so cool!"
        }
    ]
    # posts = g.user.followed_posts().all() + posts
    # start ,number, error
    per_page = app.config["POSTS_PER_PAGE"]
    posts = g.user.followed_posts().paginate(page, per_page, False)

    return render_template("index.html", title="Home", form=form, posts=posts)


@app.route("/login", methods=["GET", "POST"])
@oid.loginhandler
def login():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        print("form validate", form.openid.data)
        session['remember_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for=["nickname", "email"])
        # flash("Login requested for OpenID='" + form.openid.data + "', remember_me=" + str(form.remember_me.data))
        # return redirect("/index")
    return render_template("login.html", title="Sign In", form=form, providers=app.config['OPENID_PROVIDERS'])


@oid.after_login
def after_login(resp):
    if resp.email is None or resp.email == "":
        flash(gettext("Invalid login, Please try again."))
        return redirect(url_for("login"))
    user = User.query.filter_by(email=resp.email).first()
    if user is None:
        nickname = resp.nickname
        if nickname is None or nickname == "":
            nickname = resp.email.split("@")[0]
        nickname = User.make_unique_nickname(nickname)
        user = User(nickname=nickname, email=resp.email)
        db.session.add(user)
        db.session.commit()
        db.session.add(user.follow(user))
        db.session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop("remember_me", None)
    login_user(user, remember=remember_me)

    next_url = request.args.get("next")

    return redirect(next_url or url_for("index"))


@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated:
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()
        g.search_form = SearchForm()
    g.local = get_locale()


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))


@app.route("/user/<nickname>")
@app.route("/user/<nickname>/<int:page>")
@login_required
def user(nickname, page=1):
    user = User.query.filter_by(nickname=nickname).first()
    if user == None:
        flash("User " + nickname + " not found")
        return redirect(url_for("index"))
    per_page = app.config["POSTS_PER_PAGE"]
    posts = user.posts.paginate(page, per_page, False)
    return render_template("user.html", user=user, posts=posts)


@app.route("/edit", methods=["GET", "POST"])
@login_required
def edit():
    form = EditForm(g.user.nickname)
    if form.validate_on_submit():
        g.user.nickname = form.nickname.data
        g.user.about_me = form.about_me.data
        db.session.add(g.user)
        db.session.commit()
        flash("Your changes have been saved.")
        return redirect(url_for('edit'))
    else:
        form.nickname.data = g.user.nickname
        form.about_me.data = g.user.about_me
    return render_template("edit.html", form=form)


@app.route('/follow/<nickname>')
@login_required
def follow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash("User %s not found" % nickname)
        return redirect(url_for("index"))
    if user == g.user:
        flash("You can't follow yourself")
        return redirect(url_for("user", nickname=nickname))
    u = g.user.follow(user)
    if u is None:
        flash("Cannot follow " + nickname + ".")
    db.session.add(u)
    db.session.commit()
    flash("You are now following " + nickname + "!")
    return redirect(url_for("user", nickname=nickname))


@app.route("/unfollow/<nickname>")
@login_required
def unfollow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash("User %s not found" % nickname)
        return redirect(url_for("index"))
    if user == g.user:
        flash("You can't unfollow yourself")
        return redirect(url_for("user", nickname=nickname))
    u = g.user.unfollow(user)
    if u is None:
        flash("Cannot unfollow " + nickname + ".")
        return redirect(url_for("user", nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash("You have stopped following " + nickname + ".")
    return redirect(url_for("user", nickname=nickname))


@app.route("/search", methods=['POST'])
def search():
    if not g.search_form.validate_on_submit():
        return redirect(url_for("index"))
    return redirect(url_for("search_results", query=g.search_form.search.data))


@app.route("/search_results/<query>")
@login_required
def search_results(query):
    max_search_results = app.config["MAX_SEARCH_RESULTS"]
    results = Post.query.whoosh_search(query, max_search_results).all()
    return render_template("search_results.html",
                           query=query,
                           results=results)


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(app.config["LANGUAGES"].keys())

@app.errorhandler(404)
def outer_error(error):
    return render_template("404.html"), 404


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template("500.html"), 500
