#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/7
    Time: 9:40
"""
import os

from flask import Flask
from flask_login import LoginManager
from flask_openid import OpenID
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail

from config import ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD
from config import BASE_DIR
from momentjs import Momentjs
from flask_babel import Babel, lazy_gettext

app = Flask(__name__)
app.config.from_object("config")
app.jinja_env.globals["momentjs"] = Momentjs
if not app.debug:
    import logging
    from logging.handlers import SMTPHandler

    credentials = None
    if MAIL_USERNAME or MAIL_PASSWORD:
        credentials = (MAIL_USERNAME, MAIL_PASSWORD)
    mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + MAIL_SERVER, ADMINS, 'microBlog Failure',
                               credentials)
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
lm.login_message = lazy_gettext(u"请登录后访问本页.")

oid = OpenID(app, os.path.join(BASE_DIR, "tmp"))
db = SQLAlchemy(app)
mail = Mail(app)
babel = Babel(app)
from app import views, models
